# Laboratorio1_TW

### Explicacion general.
En el siguiente laboratorio se desarrolla la construcción de una página web la cual contiene diferentes puntos sobre información personal, tales como informacion personal, hobbies e intereses, informacion de viajes y otros. En este ultimo se desarrolla un apartado en donde se muestran los videos mas populares del 2022(estos videos no son los mas populares puesto que los que si lo eran estaban sujetos a Copyright).
Se utilizan imagenes, videos, links, formularios, tablas, gifs y distintas herramientas de HTML y CSS. 

### Desarrollo.
Para el correcto funcionamiento del sitio que se ha creado se generaron 6 archivos HTML y CSS, cada uno de estos contiene informacion sobre las paginas. 
- Archivo Lab1 y estilo -> son los encargados de la pagina principal que se ha creado el segundo archivo contiene los parametros que se han utilizado para su diseño.

- Archivo Url1 y estilo1 -> son los encargados de la pagina que contiene la informacion personal familiar que se ha creado el segundo archivo contiene los parametros que se han utilizado para su diseño.

- Archivo Url2 y estilo2 -> son los encargados de la pagina que contiene la informacion sobre hobbies e interes de una familia en donde se quiere mostrar como seria para diferentes perfiles. El segundo archivo contiene los parametros que se han utilizado para su diseño.

- Archivo Url3 y estilo3 -> son los encargados de la pagina que contiene la informacion del viaje en este caso se cuenta la experiencia personal a modo de ejemplo, el segundo archivo contiene los parametros que se han utilizado para su diseño.

- Archivo Url4 y estilo4 -> son los encargados de la pagina que contiene de otros aspectos en donde se muestran los videos mas populares, el segundo archivo contiene los parametros que se han utilizado para su diseño.

- Archivo UrlRegistro y estiloregistro -> son los encargados de la pagina del registro cuando el usuario completa la suscripcion a la pagina que se ha creado, el segundo archivo contiene los parametros que se han utilizado para su diseño.


### Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)

### Ejecutando Pruebas
Para ejecutar el laboratorio 1 propuesto se tiene que descargar cada uno de los archivos y hacer click en el archivo que se llama Lab1.html puesto que esta es la pagina principal del sitio que se ha creado y de esta manera luego se puede ir interactuando con las demas. 

## Construido con:

Ubuntu: Sistema operativo.
HTML: Lenguaje de programación.
Atom: Editor de código.
